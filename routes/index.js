const express = require('express');
const { verifySignUp, authJwt } = require("../middleware");
const authController = require("../controllers/auth.controller");
const userController = require("../controllers/user.controller");
const followerController = require("../controllers/follower.controller");
const topicController = require("../controllers/topic.controller");
const helpController = require("../controllers/help.controller");
const roomController = require("../controllers/room.controller");
const notificationController = require("../controllers/notification.controller");
const { multerUploads } = require("../utils/multerUtil");
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({ message: "PolyClub API is Running" });
});

/* Authentication */
router.post(
  "/auth/register",
  verifySignUp.checkDuplicateUsernameOrEmail,
  authController.signup
);
router.post("/auth/login", authController.signin);

/* Profile */
router.get(
  "/user/profile/me",
  authJwt.verifyToken,
  userController.myProfile
);
router.put("/user/profile/update", authJwt.verifyToken, userController.updateProfile);
router.put("/user/profile/avatar", [authJwt.verifyToken, multerUploads], userController.changeAvatar);
router.put(
  "/user/topic/choose", 
  authJwt.verifyToken,
  userController.chooseTopic
);

router.get(
  "/user/follower",
  authJwt.verifyToken,
  followerController.get_followers
);

router.get(
  "/user/following",
  authJwt.verifyToken,
  followerController.get_following
);

router.get(
  "/user/follower_by_id/:id",
  authJwt.verifyToken,
  followerController.get_followers_by_id
);

router.get(
  "/user/following_by_id/:id",
  authJwt.verifyToken,
  followerController.get_following_by_id
);
router.get(
  "/help",
  helpController.get_qna
);
router.post(
  "/help",
  helpController.add_qna
);
router.delete(
  "/help/:id",
  helpController.delete_qna
);

router.post(
  "/user/follower/add",
  authJwt.verifyToken,
  followerController.add_followers
);
router.delete(
  "/unfollows/:id", 
authJwt.verifyToken, 
followerController.unfollows);

router.get(
  "/user_by_id/:id",
  authJwt.verifyToken,
  userController.get_users_by_id
);

router.get("/user/search", userController.searchUser);

/* Room */
router.post("/room/create", authJwt.verifyToken, roomController.createRoom);
router.get("/room/all", roomController.findAllRoom);
router.get("/room/start_time", roomController.getStartTime);
router.get("/room/single/:id", roomController.findOneRoom);
router.delete("/room/single/:id", authJwt.verifyToken, roomController.deleteRoom);
router.get("/room/recommendation", authJwt.verifyToken, roomController.findRecommendationRoom);
router.put("/room/single/:id", authJwt.verifyToken, roomController.updateRoom);

router.get(
  "/room/mine",
  authJwt.verifyToken, 
  roomController.get_my_room
);

router.post("/room/reminder/:id", authJwt.verifyToken, roomController.setReminderRoom);
router.delete("/room/reminder/cancel/:roomId", authJwt.verifyToken, roomController.cancelReminderRoom);

/* Notification */
router.get("/notification/mine", authJwt.verifyToken, notificationController.findAllNotification);
router.post("/notification/basic", notificationController.createBasicNotification);

/* Resource */
router.get("/resource/topics", topicController.findAllTopic);

/* Forgot Password */
router.post("/password-reset/send", authController.requestForgotPassword);
router.post("/password-reset/reset", authController.resetPassword);

module.exports = router;