const appConfig = require('../config/app.config');
const { config, uploader } = require('cloudinary');

const cloudinaryConfig = (req, res, next) => {
  config({
    cloud_name: appConfig.CLOUDINARY_CLOUD_NAME,
    api_key: appConfig.CLOUDINARY_API_KEY,
    api_secret: appConfig.CLOUDINARY_API_SECRET,
  });

  next();
}

module.exports = { cloudinaryConfig, uploader };