const multer = require('multer');
const path = require('path');
const DatauriParser = require('datauri/parser');

const storage = multer.memoryStorage();
const multerUploads = multer({ storage }).single('image');
const dUri = new DatauriParser();

const dataUri = req => dUri.format(path.extname(req.file.originalname).toString(), req.file.buffer);

module.exports = { multerUploads, dataUri};