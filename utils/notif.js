const appConfig = require('../config/app.config');

const OneSignal = require('onesignal-node');
const appId = appConfig.ONESIGNAL_APP_ID;
const apiKey = appConfig.ONESIGNAL_API_KEY;
const client = new OneSignal.Client(appId, apiKey);

module.exports = client;