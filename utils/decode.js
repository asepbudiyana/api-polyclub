const jwt = require("jsonwebtoken")
const configAuth = require("../config/auth.config")

const getDecodedToken = (req) => {
  const tokenHeader = req.headers['authorization']
  const bearerToken = tokenHeader.split(' ')
  const token = bearerToken[1]
  const decoded = jwt.verify(token, configAuth.secret);

  return decoded
}

module.exports = getDecodedToken