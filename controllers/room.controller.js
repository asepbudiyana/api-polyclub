const getDecodedToken = require("../utils/decode");
const response = require("../utils/response");
const notif = require("../utils/notif");
const moment = require('moment');
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const db = require("../models");
const sequelize = db.sequelize;
const Room = db.room;
const User = db.user;
const Topic = db.topic;
const Follower = db.follower;
const Notification = db.notification;
const Reminder = db.reminder;

exports.createRoom = (req, res) => {
  const decoded = getDecodedToken(req);
  const isNotScheduled = (req.body.start_time === undefined) || (req.body.start_time === "");
  const nameRoom = req.body.name;
  const descriptionRoom = req.body.description;
  const topicRoom = req.body.topic_id;
  
  if (isNotScheduled == false) {
    if (Date.parse(req.body.start_time) < Date.now())
      return response.error("Cannot create scheduled room at past", res);
  }

  Follower.findAll({
    attributes: ["following_id"],
    raw: true,
    where: {
      follower_id: decoded.id
    },
  })

  .then(follower => {
      const following = follower.map((d) => d.following_id.toString());
      const notification = {
        contents: {'en': descriptionRoom},
        headings: {'en': `[${decoded.name}] Room baru : ${nameRoom}`},
        android_channel_id: '188c2a71-0ea5-4d0c-8adf-21377f81c9c0', //New Room
        android_group: 'NEW_ROOM',
        large_icon: decoded.avatar,
        include_external_user_ids: following,
        filters: [
          { field: 'tag', key: 'topic1', relation: '=', value: topicRoom },
          {operator: "OR"}, { field: 'tag', key: 'topic2', relation: '=', value: topicRoom },
          {operator: "OR"}, { field: 'tag', key: 'topic3', relation: '=', value: topicRoom },
        ]
      };

      notif.createNotification(notification)
      .then(data => {
        // Save Room to Database
        Room.create({
          name: nameRoom,
          description: descriptionRoom,
          start_time: isNotScheduled ? null : req.body.start_time,
          is_scheduled: isNotScheduled ? false : true,
          topic_id: topicRoom,
          host_id: decoded.id
        })
          .then(data => {
            response.created("Room was created successfully!", data, res);
          })
          .catch(err => response.error(err.message, res));
        })
      .catch(err => response.error(err.message, res));
  })
  .catch(err => response.error(err.message, res));
};

exports.setReminderRoom = (req, res) => {
  const decoded = getDecodedToken(req);
  const id = req.params.id;

  Room.findOne({
    where: {id: id},
    include: [
      {
        model: User,
        as: 'host',
      }],
  })
    .then(room => {
      if (Date.parse(room.start_time) < Date.now())
        response.error("Room already started", res);

      const notification = {
        contents: {'en': "Akan segera dimulai"},
        headings: {'en': room.name},
        android_channel_id: '60166102-a54a-4bb1-94c3-566f314e7d6d', //Reminder Room
        android_group: 'REMINDER_ROOM',
        large_icon: room.host.avatar,
        send_after: moment(room.start_time).format('MMMM Do YYYY, h:mm:ss a'),
        include_external_user_ids: [decoded.id.toString()],
      };

      notif.createNotification(notification)
      .then(notifData => {
        Reminder.create({
          notif_id: notifData.body.id,
          room_id: room.id,
          user_id: decoded.id,
        })
  
        .then(data => response.ok("Reminder was seted successfully!", data, res))
        .catch(err => response.error(err.message, res));
      })
      .catch(err => response.error(err.message, res));
    })
    .catch(err => response.error(err.message, res));
}

exports.cancelReminderRoom = (req, res) => {
  const decoded = getDecodedToken(req);
  const roomId = req.params.roomId;

  Reminder.findOne({
    where: {
      [Op.and]: [
        { room_id: roomId },
        { user_id: decoded.id }
      ]
    }
  })
    .then(reminderData => {
      notif.cancelNotification(reminderData.notif_id)
      .then(notifData => {
        Reminder.destroy({
          where: {
            [Op.and]: [
              { room_id: roomId },
              { user_id: decoded.id }
            ]
          }
        })
        .then(data => response.ok("Reminder was cancel successfully!", data, res))
        .catch(err => response.error(err.message, res));
      })
      .catch(err => response.error(err.message, res));
    })
    .catch(err => response.error(err.message, res));
}

exports.findAllRoom = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
    const name = req.query.name;
    var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;

    const room = await Room.findAll({ 
      where: condition,
      attributes: {
        include: [
          getQueryIsReminded(decoded.id)
        ]
      },
      include: [
        Topic, 
        {
          model: User,
          as: 'host',
          attributes: {
            include: [
              getQueryIsFollow(decoded.id),
              getQueryTotalFollowing(),
              getQueryTotalFollower(),
            ]
          },
        }],
      order: [
        ['updated_at', 'DESC']
      ]     
    });

    response.ok("Get All Room success", room, res);
  }
  catch (err) {
    response.error(err.message, res)
  }
};

exports.getStartTime = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);

    const room = await Room.findAll({ 
      where: {is_scheduled: true},
      attributes: {
        include: [
          getQueryIsReminded(decoded.id)
        ]
      },
      include: [
        Topic, 
        {
          model: User,
          as: 'host',
          attributes: {
            include: [
              getQueryIsFollow(decoded.id),
              getQueryTotalFollowing(),
              getQueryTotalFollower(),
            ]
          },
        }],
      order: [
        ['updated_at', 'DESC']
      ]      
    });

    response.ok("Get All Start Time success", room, res);
  }
  catch (err) {
    response.error(err.message, res)
  }
};

exports.findOneRoom = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
    const id = req.params.id;

    const room = await Room.findOne({
      where: {id: id},
      include: [
        Topic, 
        {
          model: User,
          as: 'host',
          attributes: {
            include: [
              getQueryIsFollow(decoded.id),
              getQueryTotalFollowing(),
              getQueryTotalFollower()
            ]
          },
        }],
    });

    if (room) return response.ok(`Get Room ${id} success`, room, res);
    else return response.notfound(`Cannot find Room ${id}.`, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.deleteRoom = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
    const id = req.params.id;

    const room = await Room.destroy({
      where: {
        [Op.and]: [
          { id: id },
          { host_id: decoded.id }
        ]
      }
    });

    if (room == 1) return response.ok("Room was deleted successfully!", "", res);
    else return response.notfound(`Cannot delete Room with id=${id}.`, res)
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.findRecommendationRoom = (req, res) => {
  const decoded = getDecodedToken(req);

  User.findByPk(decoded.id)
  .then(user => {
    if (user) {
      Room.findAll({ 
        where: {
          topic_id: {
            [Op.or]: [user.topic1_id, user.topic2_id, user.topic3_id]
          }
        },
        attributes: {
          include: [
            getQueryIsReminded(decoded.id)
          ]
        },
        include: [
          Topic, 
          {
            model: User,
            as: 'host',
            attributes: {
              include: [
                getQueryIsFollow(decoded.id),
                getQueryTotalFollowing(),
                getQueryTotalFollower(),
              ]
            },
          }],
        order: Sequelize.literal('random()')   
      })
      .then(data => response.ok("Get Recommendation Room success", data, res))
      .catch(err => response.error(err.message, res));
    } else {
      response.notfound(`Cannot find User ${decoded.id}.`, res);
    }
  })
  .catch(err => response.error(err.message, res));
};

exports.get_my_room = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);

    const room = await Room.findAll({
      where: {host_id: decoded.id},
      attributes: {
        include: [
          getQueryIsReminded(decoded.id)
        ]
      },
      include: [
        Topic, 
        {
          model: User,
          as: 'host',
          attributes: {
            include: [
              getQueryIsFollow(decoded.id),
              getQueryTotalFollowing(),
              getQueryTotalFollower(),
            ]
          },
        }],
        order: [
          ['updated_at', 'DESC']
        ]
    });

    response.ok("Get All my room success", room, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.updateRoom = (req, res) => {
  const decoded = getDecodedToken(req);
  const id = req.params.id;
  const isNotScheduled = (req.body.start_time === undefined) || (req.body.start_time === "");
      
  Room.update({
      name: req.body.name,
      description: req.body.description,
      start_time: isNotScheduled ? null : req.body.start_time,
      is_scheduled: isNotScheduled ? false : true,
      topic_id: req.body.topic_id,
  }, {
    where: {
      [Op.and]: [
        { id: id },
        { host_id: decoded.id }
      ]
    }
  })
  .then(num => {
      if (num == 1) {
        Room.findByPk(id)
          .then(data => response.created("Room was updated successfully", data, res))
          .catch(err => response.error(err.message, res));
      } else {
        response.notfound("Cannot update room, room not found", res);
      }
  })
  .catch(err => response.error(err.message, res));
};  

const getQueryIsReminded = (id) => {
  return [
    sequelize.literal(`(
      SELECT EXISTS(
        SELECT 1 
        FROM reminders AS r 
        WHERE
          r.room_id = rooms.id
        AND
          r.user_id = ${id}
        )
    )`),
    'is_reminded'
  ];
}

const getQueryIsFollow = (id) => {
  return [
    sequelize.literal(`(
      SELECT EXISTS(
        SELECT 1 
        FROM followers AS f 
        WHERE
          f.follower_id = host.id
        AND
          f.following_id = ${id}
        )
    )`),
    'is_follow'
  ];
}

const getQueryTotalFollowing = () => {
  return [
    sequelize.literal(`(
        SELECT COUNT(*)
        FROM followers AS f
        WHERE
          f.following_id = host.id
    )`),
    'total_following'
  ];
}

const getQueryTotalFollower = () => {
  return [
    sequelize.literal(`(
        SELECT COUNT(*)
        FROM followers AS f
        WHERE
          f.follower_id = host.id
    )`),
    'total_follower'
  ];
}