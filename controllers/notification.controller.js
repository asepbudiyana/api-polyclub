const getDecodedToken = require("../utils/decode");
const response = require("../utils/response");
const notif = require("../utils/notif");
const db = require("../models");
const sequelize = db.sequelize;
const Notification = db.notification;
const User = db.user;

exports.createBasicNotification = async (req, res) => {
  try {
    const notification = {
      contents: {'en': req.body.content},
      headings: {'en': req.body.title},
      large_icon: req.body.icon,
      included_segments: ['Subscribed Users']
    };
    const data = await notif.createNotification(notification);
    response.ok("Notification was created successfully!", data, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.findAllNotification = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
    const data = await Notification.findAll({ 
      where: { user_id: decoded.id},
      order: [
        ['created_at', 'DESC']
      ],
      include: [
        {
          model: User,
          as: 'following',
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT EXISTS(
                    SELECT 1 
                    FROM followers AS f 
                    WHERE
                      f.follower_id = following.id
                    AND
                      f.following_id = ${decoded.id}
                    )
                )`),
                'is_follow'
              ],
            ]
          },
        }
      ]       
    });

    response.ok("Get All Notification success", data, res);
  }
  catch(err) {
    response.error(err.message, res)
  }
};