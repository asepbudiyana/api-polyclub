const db = require("../models");
const response = require('../utils/response');
const { Op } = require("sequelize");
const Topic = db.topic;

exports.findAllTopic = async (req, res) => {
  try {
    const name = req.query.name;
    var condition = name ? { name: { [Op.like]: `%${name}%` } } : null;
  
    const topic = await Topic.findAll({ 
      where: condition,     
    });

    response.ok("Get All Topic success", topic, res);
  }
  catch (err) {
    response.error(err.message, res)
  }
};