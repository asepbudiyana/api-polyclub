const getDecodedToken = require("../utils/decode");
const response = require("../utils/response");
const notif = require("../utils/notif");
const db = require("../models");
const sequelize = db.sequelize;
const Follower = db.follower;
const User = db.user;
const Notification = db.notification;
const Op = db.Sequelize.Op;

exports.get_followers = (req, res) => {
    const decoded = getDecodedToken(req);

    Follower.findAll({
        where: {
            following_id: decoded.id,
        },
        include: [
            { 
                model: User, 
                as: 'following'
            },
        ],
        attributes: ['following.username']
    })
    
    .then(data => {
        response.looping("Get All followers success", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.get_followers_by_id = async (req, res) => {
  try {
    const id = req.params.id;
    const decoded = getDecodedToken(req);

    const follower = await Follower.findAll({
      where: {
        follower_id: id
      },
      include: [
        { 
          model: User, 
          as: 'following',
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT EXISTS(
                    SELECT 1 
                    FROM followers AS f 
                    WHERE
                      f.follower_id = following.id
                    AND
                      f.following_id = ${decoded.id}
                    )
                )`),
                'is_follow'
              ],
              [
                sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM followers AS f
                    WHERE
                      f.following_id = following.id
                )`),
                'total_following'
              ],
              [
                sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM followers AS f
                    WHERE
                      f.follower_id = following.id
                )`),
                'total_follower'
              ],
            ]
          },
        }, 
      ]
    });

    response.looping("Get All followers success", follower, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.get_following_by_id = async (req, res) => {
  try {
    const id = req.params.id;
    const decoded = getDecodedToken(req);
    
    const follower = await Follower.findAll({
      where: {
          following_id: id,
      },
      include: [
        { 
          model: User, 
          as: 'follower',
          attributes: {
            include: [
              [
                sequelize.literal(`(
                  SELECT EXISTS(
                    SELECT 1 
                    FROM followers AS f 
                    WHERE
                      f.follower_id = follower.id
                    AND
                      f.following_id = ${decoded.id}
                    )
                )`),
                'is_follow'
              ],
              [
                sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM followers AS f
                    WHERE
                      f.following_id = follower.id
                )`),
                'total_following'
              ],
              [
                sequelize.literal(`(
                    SELECT COUNT(*)
                    FROM followers AS f
                    WHERE
                      f.follower_id = follower.id
                )`),
                'total_follower'
              ],
            ]
          },
        },
      ]
    })

    response.loop("Get All following success", follower, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.add_followers = (req, res) => {
  const decoded = getDecodedToken(req);
  const follower = req.body.follower_id;
  const notification = {
    headings: {'en': 'Follower Baru'},
    contents: {'en': `${decoded.name} telah mengikuti Anda`},
    android_channel_id: 'a95c4839-c622-44f6-8433-7585de75fe7f', //New Room
    android_group: 'NEW_FOLLOWER',
    large_icon: decoded.avatar,
    include_external_user_ids: [follower.toString()],
  };

  Follower.findOne({
    where: {
      follower_id: follower,
      following_id: decoded.id,
    },
  })
  .then(followerData => {
    if (followerData) {
      return response.error("You already follow this user", res)
    } else {
      notif.createNotification(notification)
      .then(data => {
        Follower.create({
            follower_id: follower,
            following_id: decoded.id,
          })
        
          .then(data => {
            Notification.create({
              content: `${decoded.name} telah mengikuti anda`,
              icon: decoded.avatar,
              user_id: follower,
              following_id: decoded.id
            })
            .then(data => response.created("Follow user successfully!", data, res))
            .catch(err => response.error(err.message, res));
        })
        .catch(err => response.error(err.message, res));
      })
      .catch(err => response.error(err.message, res));
    }
  })
  .catch(err => response.error(err.message, res));
};

exports.get_following = (req, res) => {
    const decoded = getDecodedToken(req);

    Follower.findAll({
        where: {
            follower_id: decoded.id,
        },
        include: [
            { 
                model: User, 
                as: 'follower'
            }, 
        ],
        attributes: ['follower.username']
    })
    
    .then(data => {
        response.loop("Get All following success", data, res);
    })
    .catch(err => {
        response.error(err.message, res);
    });
};

exports.unfollows = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
    const id = req.params.id;
    const follower = await Follower.destroy({
      where: {
        [Op.and]: [
          { following_id: decoded.id },
          { follower_id: id }
        ]
      }
    });
    
    if (follower) return response.ok("Unfollow successfully!", "", res);
    else return response.notfound(`Cannot unfollow with id=${id}.`, res)
  }
  catch (err) {
    response.error(err.message, res)
  }
};