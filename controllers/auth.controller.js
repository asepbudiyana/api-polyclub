const response = require("../utils/response");
const getDecodedToken = require("../utils/decode");
const sendEmail = require("../utils/sendEmail");
const config = require("../config/auth.config");
const db = require("../models");
const User = db.user;
const Token = db.token;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const gpc = require('generate-pincode')

exports.signup = async (req, res) => {
  try {
    const user = await User.create({
      username: req.body.username,
      name: req.body.name,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8)
    });

    const data = getAuthResponse(user);
    response.created("User was registered successfully!", data, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.signin = async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        username: req.body.username
      }
    });
    if (!user) return response.notfound("User not found.", res);

    var passwordIsValid = bcrypt.compareSync(
      req.body.password,
      user.password
    );
    if (!passwordIsValid) return response.unauthorized("Invalid Password!", res);
    
    const data = getAuthResponse(user);
    response.ok("User was login successfully!", data, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.requestForgotPassword = async (req, res) => {
  try {
    const user = await User.findOne({ 
      where: {
        email: req.body.email 
      },
    });

    if (!user) 
      return response.bad("user with given email doesn't exist", res);
    
    let token = await Token.findOne({ 
      where: {
        user_id: user.id 
      }
    });
    if (!token) {
      token = await Token.create({
        user_id: user.id,
        token: gpc(6)
      });
    }
    const link = `Kode untuk reset password anda : ${token.token}`;
    await sendEmail(user.email, "Password reset", link);

    response.ok("password reset link sent to your email account", "", res)
  } catch (error) {
    console.log(error);
    response.error("An error occured", res);
  }
};

exports.resetPassword = async (req, res) => {
  try {
    const user = await User.findOne({ 
      where: {
        email: req.body.email 
      },
    });
    if (!user) return response.bad("user not found", res)

    const token = await Token.findOne({
      where: {
        user_id: user.id,
        token: req.body.token,
      }
    });
    if (!token) return response.bad("invalid token or expired", res)
    
    user.password = bcrypt.hashSync(req.body.password, 8);
    await user.save();
    await token.destroy();

    response.ok("password reset sucessfully.", "", res)
  } catch (error) {
    response.error("An error occured", res);
    console.log(error);
  }
};

const getAuthResponse = (user) => {
  const token = jwt.sign({ 
    id: user.id, 
    avatar: user.avatar, 
    name: user.name 
  }, config.secret, {});

  return {
    'token': token,
    // 'expires_in': 86400,
    'user': user 
  };
}