const bcrypt = require('bcryptjs');
const cloudinary = require('cloudinary');
const getDecodedToken = require("../utils/decode");
const { dataUri } = require("../utils/multerUtil");
const response = require("../utils/response");
const notif = require("../utils/notif");
const { Op } = require("sequelize");
const db = require("../models");
const sequelize = db.sequelize;
const User = db.user;

exports.myProfile = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
  
    const user = await User.findOne({
      where: {id: decoded.id},
      attributes: {
        include: [
          getQueryTotalFollowing(decoded.id),
          getQueryTotalFollower(decoded.id),
        ]
      },
      include: getQueryIncludeTopic()
    });

    response.ok("Get profile data successfully!", user, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.changeAvatar = (req, res) => {
  const decoded = getDecodedToken(req);

  if (req.file) {
    const file = dataUri(req).content;
    return cloudinary.v2.uploader.upload(file, {folder: 'polyclub/'}).then((result) => {
      const image = result.secure_url;

      User.update({avatar: image}, 
      {
        where: { id: decoded.id }
      })
      .then(num => {
          if (num == 1) {
              User.findByPk(decoded.id)
              .then(data => response.created("Profil picture was updated successfully", data, res))
              .catch(err => response.error(err.message, res));
          } else {
              response.notfound("Cannot update profile, user not found", res);
          }
      })
      .catch(err => response.error(err.message, res));
    })
    .catch((err) => response.error(err.message, res));
  }
}

exports.updateProfile = (req, res) => {
  const decoded = getDecodedToken(req);

  var userData = {
    email: req.body.email,
    name: req.body.name,
    username: req.body.username,
    bio: req.body.bio
  }

  if (req.body.password) 
    userData.password = bcrypt.hashSync(req.body.password, 8);

  User.update(userData, {
      where: { id: decoded.id }
  })
  .then(num => {
      if (num == 1) {
          User.findByPk(decoded.id)
          .then(data => response.created("Profil was updated successfully", data, res))
          .catch(err => response.error(err.message, res));
      } else {
          response.notfound("Cannot update profile, user not found", res);
      }
  })
  .catch(err => response.error(err.message, res));
};  

exports.chooseTopic = (req, res) => {
  const decoded = getDecodedToken(req);
  const topic1 = req.body.topic1;
  const topic2 = req.body.topic2;
  const topic3 = req.body.topic3;

  // Update tag notif user
  const notification = {
    tags: {
      topic1: topic1,
      topic2: topic2,
      topic3: topic3
    },
  };

  notif.editTagsWithExternalUserIdDevice(decoded.id, notification)
  .then(data => {
    User.update({
      topic1_id: topic1,
      topic2_id: topic2,
      topic3_id: topic3,
    }, {
        where: { id: decoded.id }
    })
    .then(num => {
        if (num == 1) {
          User.findOne({
            where: {id: decoded.id},
            include: getQueryIncludeTopic()
          })
            .then(data => response.created("Topic was choose successfully", data, res))
            .catch(err => response.error(err.message, res));
        } else {
            response.notfound("Cannot choose topic, user not found" + num, res);
        }
    })
    .catch(err => response.error(err.message, res));
  })
  .catch(err => response.error(err.message, res));
};  

exports.get_users_by_id = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
    const userId = req.params.id;
  
    const user = await User.findAll({
      where: {id: userId},
      attributes: {
        include: [
          getQueryIsFollow(userId, decoded.id),
          getQueryTotalFollower(userId),
          getQueryTotalFollowing(userId)
        ]
      },
    });

    response.ok("Get All followers success", user, res);
  }
  catch (err) {
    response.error(err.message, res);
  }
};

exports.searchUser = async (req, res) => {
  try {
    const decoded = getDecodedToken(req);
    const name = req.query.name;

    const user = await User.findAll({  
      attributes: {
        include: [
          [
            sequelize.literal(`(
              SELECT EXISTS(
                SELECT 1 
                FROM followers AS f 
                WHERE
                  f.follower_id = users.id
                AND
                  f.following_id = ${decoded.id}
                )
            )`),
            'is_follow'
          ],
          [
            sequelize.literal(`(
                SELECT COUNT(*)
                FROM followers AS f
                WHERE
                  f.following_id = users.id
            )`),
            'total_following'
          ],
          [
            sequelize.literal(`(
                SELECT COUNT(*)
                FROM followers AS f
                WHERE
                  f.follower_id = users.id
            )`),
            'total_follower'
          ],
        ]
      },
      where: {
        [Op.or]: [
          { name: { [Op.iLike]: `%${name}%` } },
          { username: { [Op.iLike]: `%${name}%` } }
        ]
      },
      include: getQueryIncludeTopic()
    });

    response.ok("Search user success", user, res)
  }
  catch (err) {
    response.error(err.message, res)
  }
};

const getQueryIsFollow = (follower, following) => {
  return [
    sequelize.literal(`(
      SELECT EXISTS(
        SELECT 1 
        FROM followers AS f 
        WHERE
          f.follower_id = ${follower}
        AND
          f.following_id = ${following}
        )
    )`),
    'is_follow'
  ];
}

const getQueryTotalFollowing = (id) => {
  return [
    sequelize.literal(`(
        SELECT COUNT(*)
        FROM followers AS f
        WHERE
          f.following_id = ${id}
    )`),
    'total_following'
  ];
}

const getQueryTotalFollower = (id) => {
  return [
    sequelize.literal(`(
        SELECT COUNT(*)
        FROM followers AS f
        WHERE
          f.follower_id = ${id}
    )`),
    'total_follower'
  ];
}

const getQueryIncludeTopic = () => {
  return [
    {model: db.topic, as: 'topic1'}, 
    {model: db.topic, as: 'topic2'}, 
    {model: db.topic, as: 'topic3'}
  ]
}