const response = require("../utils/response");
const db = require("../models");
const Help = db.help;

exports.get_qna = async (req, res) => {
  try {
    const help = await Help.findAll();
    response.ok("Get All helps success", help, res);
  } 
  catch(err) {
    response.error(err.message, res);
  }
};

exports.add_qna = async (req, res) => {
  try {
    const help = await Help.create({
      pertanyaan: req.body.pertanyaan,
      jawaban: req.body.jawaban,
    });

    response.created("Help was created successfully!", help, res);
  } 
  catch(err) {
    response.error(err.message, res);
  }
};
   
exports.delete_qna = async (req, res) => {
  try {
    const id = req.params.id;
    const help = await Help.destroy({
      where: { id: id },
    })
    if (help) return response.ok("QnA was deleted successfully!", "", res);
    else return response.notfound(`Cannot delete QnA with id=${id}.`, res);
  }
  catch (err) {
    response.error(err.message, res)
  }
};