const db = require("../models");
const Topic = db.topic;

db.sequelize.sync().then(() => {
  const topics = ["Game", "Kesehatan", "Sosial", "Fashion", "Music"];
  console.log('Seed Topic Db');
  topics.forEach(initial);
});

function initial(item) {
  Topic.create({
    name: item,
    icon: 'default_icon.png'
  });
}

