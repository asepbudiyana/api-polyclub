module.exports = (sequelize, Sequelize) => {
    const Help = sequelize.define("help", {
      pertanyaan: {
        type: Sequelize.STRING
      },
      jawaban: {
        type: Sequelize.STRING
      }
    });
  
    return Help;
  };
  