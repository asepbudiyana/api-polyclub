module.exports = (sequelize, Sequelize) => {
  const Notification = sequelize.define("notifications", {
    content: {
      type: Sequelize.STRING
    },
    icon: {
      type: Sequelize.STRING
    }
  }, {
    underscored: true
  });

  return Notification;
}