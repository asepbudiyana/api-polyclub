module.exports = (sequelize, Sequelize) => {
  const Topic = sequelize.define("topics", {
    name: {
      type: Sequelize.STRING
    },
    icon: {
      type: Sequelize.STRING
    }
  }, {
    underscored: true
  });

  return Topic;
}