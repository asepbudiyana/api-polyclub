module.exports = (sequelize, Sequelize) => {
  const Token = sequelize.define("tokens", {
    token: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    tokenExpires : {
      type: Sequelize.DATE,
      defaultValue: Date.now() +  43200
    }
  }, {
    underscored: true
  });

  return Token;
}