module.exports = (sequelize, Sequelize) => {
    const Follower = sequelize.define("followers", {
      timestamp: {
        type: Sequelize.STRING
      },
      following_id: {
        type: Sequelize.INTEGER
      },
      follower_id: {
        type: Sequelize.INTEGER
      }
    });
  
    return Follower;
  };
  