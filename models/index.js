const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD,
  {
    host: config.HOST,
    dialect: config.dialect,
    dialectOptions: config.dialectOptions,
    operatorsAliases: false,

    pool: {
      max: config.pool.max,
      min: config.pool.min,
      acquire: config.pool.acquire,
      idle: config.pool.idle
    }
  }
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.follower = require("../models/followers.model.js")(sequelize, Sequelize);
db.topic = require("../models/topic.model.js")(sequelize, Sequelize);
db.room = require("../models/room.model.js")(sequelize, Sequelize);
db.help = require("../models/help.model.js")(sequelize, Sequelize);
db.notification = require("../models/notification.model.js")(sequelize, Sequelize);
db.reminder = require("../models/reminder.model.js")(sequelize, Sequelize);
db.token = require("../models/token.model.js")(sequelize, Sequelize);

db.user.belongsTo(db.topic, {foreignKey: 'topic1_id', as: 'topic1'});
db.user.belongsTo(db.topic, {foreignKey: 'topic2_id', as: 'topic2'});
db.user.belongsTo(db.topic, {foreignKey: 'topic3_id', as: 'topic3'});

db.room.belongsTo(db.topic, {foreignKey: "topic_id"});
db.room.belongsTo(db.user, {foreignKey: "host_id", as:'host'});
db.follower.belongsTo(db.user, {foreignKey: 'following_id', as: 'following'});
db.follower.belongsTo(db.user, {foreignKey: 'follower_id', as: 'follower'});

db.notification.belongsTo(db.user, {foreignKey: "user_id", as:'user'});
db.notification.belongsTo(db.user, {foreignKey: "following_id", as:'following'});
db.reminder.belongsTo(db.user, {foreignKey: "user_id", as:'user'});
db.reminder.belongsTo(db.room, {foreignKey: "room_id", as:'room'});
db.token.belongsTo(db.user, {foreignKey: "user_id", as:'user'});

// db.follower.belongsTo(db.user, {foreignKey: "follower_id", as:"follower"});

// db.follower.belongsTo(db.user, {foreignKey: "following_id", as:"following"});
module.exports = db;