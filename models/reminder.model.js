module.exports = (sequelize, Sequelize) => {
  const Reminder = sequelize.define("reminders", {
    notif_id: {
      type: Sequelize.STRING
    }
  }, {
    underscored: true
  });

  return Reminder;
}