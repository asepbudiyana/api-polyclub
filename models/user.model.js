const config = require("../config/app.config");

module.exports = (sequelize, Sequelize) => {
  const User = sequelize.define("users", {
    username: {
      type: Sequelize.STRING
    },
    name: {
      type: Sequelize.STRING,
      unique: true
    },
    email: {
      type: Sequelize.STRING,
      unique: true
    },
    password: {
      type: Sequelize.STRING
    },
    bio: {
      type: Sequelize.TEXT
    },
    avatar: {
      type: Sequelize.STRING
    }
  }, {
    underscored: true
  });

  User.prototype.toJSON =  function () {
    var values = Object.assign({}, this.get());
    
    delete values.password;
    return values;
  }

  return User;
};
