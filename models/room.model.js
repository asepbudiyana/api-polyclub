const config = require("../config/app.config");

module.exports = (sequelize, Sequelize) => {
  const Room = sequelize.define("rooms", {
    id: {
      type: Sequelize.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING,
      unique: true
    },
    description: {
      type: Sequelize.TEXT
    },
    is_scheduled: {
      type: Sequelize.BOOLEAN
    },
    start_time: {
      type: Sequelize.DATE,
      allowNull: true,
    }
  }, {
    underscored: true
  });

  return Room;
};
